#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2017-2018 James Clark <james.clark@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""Whiten hwinj files from pycbc_generate_hwinj and truncate/align the
timeseries to match a given analysis segment.  This script moves a collection of
hwinjs into a convenient directory structure for further processing.
"""

import sys,os,shutil
import glob
import numpy as np
from scipy import signal
import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt

import argparse

import pycbc.types

import gw_reconstruct as gwr


def parser():
    """ 
    Parser for input (command line and ini file)
    """

    # --- cmd line
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("--srate", type=int, default=2048)
    parser.add_argument("--duration", type=float, default=4.0)
    parser.add_argument("--make-plots", default=False, action="store_true")
    parser.add_argument("--bw-parent-path", default=None)
    parser.add_argument("--hwinj-pattern", default="hwinjcbcsimid")
    parser.add_argument("--hwinj-path", default=None)
    parser.add_argument("--phase-shift", default=False, action="store_true")

    opts = parser.parse_args()

    return opts

opts = parser()

#
# Locate injection files and target output
#
outpaths=[x[0] for x in os.walk(opts.bw_parent_path)][1:]
hwinjfiles=glob.glob(os.path.join(opts.hwinj_path, opts.hwinj_pattern)+'*')
hwinj_epochs=np.unique(np.array([int(os.path.basename(hwinjfile).split('_')[1])
    for hwinjfile in hwinjfiles]))
trigtimes=[os.path.basename(outpath) for outpath in outpaths]
trigtimes=np.array([int(os.path.basename(outpath)) for outpath in outpaths])


ifos = ['H1', 'L1']

#
# Process injections
#
for simid, (trigtime,outpath) in enumerate(zip(trigtimes, outpaths)):

    # Ensure unit amplitude noise
    seglen = int(opts.srate*opts.duration)
    fac = 1./opts.srate * np.sqrt(float(seglen)/2)

    # --- Identify hwinj file for this trigtime
    #hwinj_epoch = hwinj_epochs[np.argmin(abs(trigtime-hwinj_epochs))]
    hwinj_epoch = hwinj_epochs[simid]

    print "attempting to manipulate {}".format(hwinj_epoch)
    infile_fmt = opts.hwinj_pattern+'{simid}_{epoch}_{ifo}.txt'
    hwinj_data = [np.loadtxt(
        infile_fmt.format(simid=simid,epoch=hwinj_epoch,ifo=ifo)) for ifo in
        ifos]
    

    # --- Trim down timeseries
    hwinj_times = np.arange(hwinj_epoch,
            hwinj_epoch+len(hwinj_data[0])*1./opts.srate, 1./opts.srate)

    center = np.argmin(abs(hwinj_times - trigtime))
    start = int(center - 0.5*seglen)
    stop = int(center + 0.5*seglen)
    idx = range(start,stop)

    hwinj_data = [hdata[idx] for hdata in hwinj_data]

    # --- Whiten
    print "whitening {}".format(hwinj_epoch)

    # Load PSDs for whitening
    bwpath = os.path.join(opts.bw_parent_path, str(int(trigtime)))
    psd_infile_fmt = os.path.join(bwpath, 'IFO{}_psd.dat')
    infiles = [psd_infile_fmt.format(ifo) for ifo in xrange(len(ifos))]
    psds = [gwr.psd.interp_from_txt(infile, flow=10) for infile in infiles]

    hwinj_epoch = hwinj_times[idx][0]
    hwinj_hfs = [gwr.Strain(pycbc.types.TimeSeries(hw, delta_t=1./opts.srate,
        epoch=hwinj_epoch), white=False) for hw in hwinj_data]
    hwinj_hts = [h.to_timeseries() for h in hwinj_hfs]
    hwinj_hts_white = [fac*gwr.whiten_strain(hwinj, psd).to_timeseries()
            for hwinj,psd in zip(hwinj_hts,psds)]

    for i,ifo in enumerate(ifos):
        fbasename="{ifo}-pycbchwinj-{epoch}.txt".format(ifo=ifo,
                epoch=hwinj_epoch)
        fname=os.path.join(outpath,fbasename)
        np.savetxt(fname, X=hwinj_hts_white[i].data)



