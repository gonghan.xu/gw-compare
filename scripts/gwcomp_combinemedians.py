#!/usr/bin/env python
# -*- coding:utf-8 -*-
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Authors:
# - James Alexander Clark, <james.clark@ligo.org>, 2018-2019
"""Iterate through a list of BayesWave output directories and combine the
median waveform reconstructions into a single file"""

import os
import argparse
import ConfigParser
import glob
import numpy as np

IFO_DICT = {"H1":"0", "L1":"1", "V1":"2"}

def ifo_label(ifo):
    """Return IFO label used in filename
    IFO_DICT={"H1":"0", "L1":"1", "V1":"2"}"""
    try:
        return IFO_DICT[ifo]
    except KeyError:
        print "IFO %s not supported.  Use: ", IFO_DICT

def parse_cmdline():
    """Parse command line"""

    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("workdir", type=str, default=None, nargs='?',
                        help="""Parent directory for BayesWave run""")
    parser.add_argument("-r", "--results-pattern", type=str,
                        default="trigtime*", help="""Output directories grep pattern""")
    parser.add_argument("-s", "--save-name", type=str, default="posterior_draw_reconstructions",
                        help="""Name of output file to store results""")
    parser.add_argument("-p", "--plots", action="store_true", default=False,
                        help="""Make plots and plots of merit""")
    parser.add_argument("-i", "--ifo", type=str, default="H1",
                        help="""Interferometer reconstruction to use""")
    parser.add_argument("-m", "--medians_file", type=str,
                        default="signal_median_time_domain_waveform.dat",
                        help="""Interferometer reconstruction to use""")
    parser.add_argument("-w", "--save-wave-array", action="store_true",
                        default=False,
                        help="""Dump the full array of median waveforms to disk""")
    args = parser.parse_args()

    config = ConfigParser.ConfigParser()
    config.optionxform = str
    config.read(os.path.join(args.workdir, "config.ini"))

    return args, config

## Input locations and configuration
ARGS, CONFIG = parse_cmdline()
SEGLEN = CONFIG.getfloat('input', 'seglen')
SRATE = CONFIG.getfloat('input', 'srate')
WAVELEN = int(SEGLEN*SRATE)

## Load median time-domain waveforms
ALLJOBS = glob.glob(os.path.join(ARGS.workdir, ARGS.results_pattern))

## Identify output directories with successful post jobs
JOBS = []
for JOB in enumerate(ALLJOBS):
    wavefile = os.path.join(JOB[1], "post", ARGS.medians_file+'.'+ifo_label(ARGS.ifo))
    if os.path.exists(wavefile) and os.stat(wavefile).st_size>0:
        JOBS.append(JOB[1])

WAVEARRAY = np.zeros(shape=(len(JOBS), WAVELEN))
for j, JOB in enumerate(JOBS):
    print "Loading %d of %d"%(j+1, len(JOBS))
    wavefile = os.path.join(JOB, "post", ARGS.medians_file+'.'+ifo_label(ARGS.ifo))
    try:
        WAVEARRAY[j, :] = np.genfromtxt(wavefile, usecols=1)
    except:
        continue

## Figures of Merit
WAVECHARS = np.percentile(WAVEARRAY, q=(5, 50, 95), axis=0)

## Add time axis and save output
TIME = np.arange(0, SEGLEN, 1./SRATE)
if ARGS.save_wave_array:
    print "Saving all medians"
    HEAD = "# time_s " + len(WAVEARRAY)*" reconstruction"
    np.savetxt(ARGS.save_name+"_medians.txt.gz", np.vstack([TIME, WAVEARRAY]).T,
               header=HEAD)
print "Saving waveform intervals"
HEAD = "# time_s percentile_5 percentile_50 percentile_95"
np.savetxt(ARGS.save_name+"_intervals.txt.gz", np.vstack([TIME, WAVECHARS]).T,
           header=HEAD)

## Plot Intervals
if ARGS.plots:
    TIME -= TIME[int(0.5*len(TIME))]
    from matplotlib import pyplot as plt
    FIG, AX = plt.subplots()
    AX.fill_between(TIME, y1=WAVECHARS[0], y2=WAVECHARS[2], label='90% CI')
    AX.plot(TIME, WAVEARRAY.T, alpha=0.5, color='grey', linewidth=0.5)
    AX.set_xlim(-0.1, 0.1)
    AX.legend(loc="upper left")
    plt.savefig(ARGS.save_name+'.png')
